![Bandwidth Banner](./src/images/bandwidthrevolt-banner.png)
___
Bandwidth for Revolt is the Pretendo Network Discord community bot but for Revolt!
(Please note that this is port of the bot is not officially from Pretendo.)

Bandwidth can:
- Read certain error codes, and give information on them
- Submit reports and moderator applications
- Show channels with live member counts that update every 5 minutes
- And more!
